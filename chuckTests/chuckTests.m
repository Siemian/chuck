//
//  chuckTests.m
//  chuckTests
//
//  Created by Siemian on 05.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SMChuckApiClient.h"

@interface chuckTests : XCTestCase

@end

@implementation chuckTests

-(void)testDownloadingJoke
{
    XCTestExpectation *getFactExpectation = [self expectationWithDescription:@"get fact"];
    
    [[SMChuckApiClient sharedInstance] nextJoke:^(id result,NSError *error) {
        

        XCTAssertFalse(!result && !error);
        [getFactExpectation fulfill];
    }];
    
    //defaulf afnetwroking timeout 10s
    [self waitForExpectationsWithTimeout:12 handler:^(NSError *error) {
        
    }];
    
}

-(void)testDownloadingMemeImage
{
    XCTestExpectation *getImageExpectation = [self expectationWithDescription:@"get fact"];

    [[SMChuckApiClient sharedInstance] createMemeWithTopText:@"aaa  a" bottomText:@"ads sa as da d" result:^(id result, NSError *error) {
        XCTAssertFalse(!result && !error);
        
        XCTAssertFalse(result && ![result isKindOfClass:[UIImage class]]);
        [getImageExpectation fulfill];
    }];
    
    
    [self waitForExpectationsWithTimeout:12 handler:^(NSError *error) {
    
    }];
}

@end
