//
//  SMViewController.h
//  chuck
//
//  Created by Siemian on 05.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *jokeLabel;
- (IBAction)nextPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *chuckLoading;

@end
