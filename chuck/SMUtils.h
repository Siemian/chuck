//
//  SMUtils.h
//  chuck
//
//  Created by Siemian on 05.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMUtils : NSObject

+(void)showAlertWithTitle:(NSString*)title message:(NSString*)msg;
+(void)applyChuckAnimation:(CALayer*)layer;
@end
