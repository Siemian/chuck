//
//  SMShowMemeViewController.h
//  chuck
//
//  Created by Siemian on 05.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMShowMemeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property(strong,nonatomic) NSString *topText;
- (IBAction)sharePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *chuckLoading;
@property(strong,nonatomic) NSString *bottomText;
@end
