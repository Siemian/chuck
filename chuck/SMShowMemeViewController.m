//
//  SMShowMemeViewController.m
//  chuck
//
//  Created by Siemian on 05.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import "SMShowMemeViewController.h"
#import "SMChuckApiClient.h"
#import "SMUtils.h"

@interface SMShowMemeViewController ()

@end

@implementation SMShowMemeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getMeme];
    [SMUtils applyChuckAnimation:_chuckLoading.layer];
   
    
}

-(void)getMeme
{
    _bottomText=[_bottomText stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    _topText=[_topText stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    
    [[SMChuckApiClient sharedInstance] createMemeWithTopText:_topText bottomText:_bottomText result:^(id result, NSError *error) {
        [_chuckLoading setHidden:YES];
        if(result)
            _imageView.image=result;
    }];
    
}

- (IBAction)sharePressed:(id)sender
{
    NSString *text = @"Chuck is the King";
    UIImage *image = _imageView.image;
    NSArray *activityItems = @[text, image];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint, UIActivityTypePostToTwitter, UIActivityTypePostToWeibo];
    [self presentViewController:activityVC animated:TRUE completion:nil];

}
@end
