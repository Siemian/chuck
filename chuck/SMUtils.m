//
//  SMUtils.m
//  chuck
//
//  Created by Siemian on 05.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import "SMUtils.h"

@implementation SMUtils

+(void)showAlertWithTitle:(NSString*)title message:(NSString*)msg
{
    [[[UIAlertView alloc]initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

+(void)applyChuckAnimation:(CALayer*)layer
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0  ];
    rotationAnimation.duration = 1;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = HUGE_VALF;
    
    [layer addAnimation:rotationAnimation forKey:@"rotate"];
}
@end
