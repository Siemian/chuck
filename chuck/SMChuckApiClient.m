//
//  SMChuckApiClient.m
//  chuck
//
//  Created by Siemian on 05.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import "SMChuckApiClient.h"
#import <AFHTTPRequestOperationManager.h>
#import "SMUtils.h"



@interface SMChuckApiClient()

@property(strong,nonatomic) AFHTTPRequestOperationManager *manager;

@end

@implementation SMChuckApiClient

+(SMChuckApiClient*)sharedInstance {
    static SMChuckApiClient *sharedI = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedI = [[self alloc] init];
    });
    sharedI.manager=[[AFHTTPRequestOperationManager alloc] init];
    
    return sharedI;
}

-(void)nextJoke:(void(^)(id result,NSError *error))result
{
    [_manager GET:@"http://api.icndb.com/jokes/random?limitTo=[nerdy]" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        if([responseObject isKindOfClass:[NSDictionary class]] && [[responseObject objectForKey:@"type"] isEqualToString:@"success"])
        {
            NSString *joke=[[responseObject objectForKey:@"value"] objectForKey:@"joke"];
            result(joke,nil);
        }
        else
            result(nil,nil);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        result(nil,error);
        [SMUtils showAlertWithTitle:@"Internet error" message:@"Chuck is offline"];
        
    }];
    
}

-(void)createMemeWithTopText:(NSString*)topText bottomText:(NSString*)bottomText result:(void(^)(id result,NSError *error))resultBlock
{
    
    NSString *urlString=[NSString stringWithFormat:@"http://apimeme.com/meme?meme=3g3xw&top=%@&bottom=%@",topText,bottomText];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
  
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFImageResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if([responseObject isKindOfClass:[UIImage class]])
        {
            resultBlock(responseObject,nil);
        }
        else
            resultBlock(nil,nil);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SMUtils showAlertWithTitle:@"Internet error" message:@"Chuck is offline"];
        resultBlock(nil,error);
    }];
    
    [operation start];
}

@end
