//
//  SMViewController.m
//  chuck
//
//  Created by Siemian on 05.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import "SMViewController.h"
#import "SMChuckApiClient.h"
#import "SMMemeViewController.h"
#import "SMUtils.h"

@interface SMViewController ()

@end

@implementation SMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    
   
    
    [SMUtils applyChuckAnimation:_chuckLoading.layer];
}

-(void)viewDidAppear:(BOOL)animated
{
    if([_jokeLabel.text isEqualToString:@"Chuck"])
        [self nextJoke];
}


-(void)nextJoke
{
    _jokeLabel.text=@"";
    [_chuckLoading setHidden:NO];
    
    [[SMChuckApiClient sharedInstance] nextJoke:^(id result,NSError *error) {
        _jokeLabel.text=result;
        [_chuckLoading setHidden:YES];
    }];
}

- (IBAction)nextPressed:(id)sender {
     [self nextJoke];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"createMeme"])
    {
        SMMemeViewController *dst=segue.destinationViewController;
        dst.fact=_jokeLabel.text;
    }
}
@end
