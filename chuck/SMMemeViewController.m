//
//  SMMemeViewController.m
//  chuck
//
//  Created by Siemian on 05.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import "SMMemeViewController.h"
#import "SMShowMemeViewController.h"

@interface SMMemeViewController ()<UITextViewDelegate>

@end

@implementation SMMemeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self fillData];
}

-(void)fillData
{
    NSRange r=[_fact rangeOfString:@"."];
    
    if(r.location!=NSNotFound && r.location<_fact.length-1)
    {
        _topText.text=[_fact substringToIndex:r.location+1];
        _bottomText.text=[_fact substringFromIndex:r.location+1];
    }
    else
    {
        _topText.text=_fact;
        _bottomText.text=_fact;
    }
        
}

- (BOOL)textView:(UITextView*) textView shouldChangeTextInRange: (NSRange) range replacementText: (NSString*) text
{
    if ([text isEqualToString:@"\n"]) {
        UIView *view = [self.view viewWithTag:textView.tag + 1];
        if (!view)
            [textView resignFirstResponder];
        else
            [view becomeFirstResponder];
        return NO;
    }
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showMeme"])
    {
        SMShowMemeViewController *dst=segue.destinationViewController;
        dst.topText=_topText.text;
        dst.bottomText=_bottomText.text;
    }
}


@end
