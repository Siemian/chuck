//
//  SMMemeViewController.h
//  chuck
//
//  Created by Siemian on 05.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMMemeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *topText;
@property (weak, nonatomic) IBOutlet UITextView *bottomText;
@property(strong,nonatomic) NSString *fact;
@end
