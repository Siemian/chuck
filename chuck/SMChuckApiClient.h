//
//  SMChuckApiClient.h
//  chuck
//
//  Created by Siemian on 05.07.2014.
//  Copyright (c) 2014 siemian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMChuckApiClient : NSObject

+(SMChuckApiClient*)sharedInstance;


-(void)createMemeWithTopText:(NSString*)topText bottomText:(NSString*)bottomText result:(void(^)(id result,NSError *error))resultBlock;
-(void)nextJoke:(void(^)(id result,NSError *error))result;

@end
